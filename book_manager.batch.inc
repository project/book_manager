<?php

/**
 * @file: function for book_manager batch operations
 */


/**
 * This function does all of the necessary setup to be called in
 * a batch context.
 * This function is intended to be called on each node in a book when
 * any node in that book is called. It is the responsibility of the
 * module implementing hook_book_manager_batch_operate() to determine
 * whether or not it wants to adjust the given node.
 * hook_book_manager_batch_operate() will be called on each node in a
 * book with the changed node as the first parameter.
 */
function book_manager_batch_process($node, &$context) {
  // do our initial batch setup
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['nids'] = book_manager_get_book_nids($node->book['bid']);
    $context['sandbox']['max'] = count($context['sandbox']['nids']);
    $context['sandbox']['functions'] = array();
    $modules = module_implements('book_manager_batch_operate');
    foreach ($modules as $module) {
      $context['sandbox']['functions'][] = $module . '_' . 'book_manager_batch_operate';
    }

    if (empty($context['sandbox']['functions'])) {
      // no functions to call, let's short circuit the batch op.
      $context['sandbox']['max'] = 0;
    }
  }
  // Do our batch operations
  $context['sandbox']['current_node'] = array_shift($context['sandbox']['nids']);
  if (!empty($context['sandbox']['current_node'])) {
    // skip the node that triggered the batch op
    if ($context['sandbox']['current_node'] != $node->nid) {
      $node2 = node_load($context['sandbox']['current_node']);
      foreach ($context['sandbox']['functions'] as $function) {
        $context['message'] = $node->title;
        call_user_func_array($function, array($node, $node2));
      }

      // let book_manager be aware we are already batch updating the nodes in this book
      // ie. avoid trying to kick off another batch on this update.
      $node2->book_manager_in_batch = TRUE;
      node_save($node2);
      $context['results'][] = $node2->title;
    }
    $context['sandbox']['progress']++;
  }

  // Adjust our batch progress
  if ($context['sandbox']['max'] != 0) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
  else {
    $context['finished'] = 1;
  }
}

/**
 * Implementation of hook_book_manager_batch_operate on behalf of og
 */
if (module_exists('og') && !function_exists('og_book_manager_batch_operate')) {
  function og_book_manager_batch_operate($node, &$node2) {
    $node2->og_groups = $node->og_groups;
  }
}

/**
 * Implementation of hook_book_manager_batch_operate on behalf of og_access
 */
if (module_exists('og_access') && !function_exists('og_access_book_manager_batch_operate')) {
  function og_access_book_manager_batch_operate($node, &$node2) {
    $node2->og_public = $node->og_public;
  }
}

/**
 * Implementation of hook_book_manager_batch_operate on behalf of og_access_roles
 */
if (module_exists('og_access_roles') && !function_exists('og_access_roles_book_manager_batch_operate')) {
  function og_access_roles_book_manager_batch_operate($node, &$node2) {
    $node2->og_access_roles = $node->og_access_roles;
  }
}
