<?php

/**
 * Builds and returns the book manager settings form.
 */
function book_manager_admin_settings($form, &$form_state) {
  $form['book_manager_show_create_links_for_each_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show create links for each allowed book content type'),
    '#default_value' => variable_get('book_manager_show_create_links_for_each_type', 0),
    '#description' => t('The book module adds a standard <em>Add child page</em> link to nodes that are in books.  Turning on this feature will replace the <em>Add child page</em> link with <em>Add [content-type]</em> links (where <em>[content-type]</em> will be replaced by the human-readable content type name) for each content type that is configured to appear in a book.'),
    '#required' => TRUE,
  );
  $form['book_manager_hide_outline_tab'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide the outline tab on all books'),
    '#default_value' => variable_get('book_manager_hide_outline_tab', 0),
    '#description' => t('The <em>Customize Book</em> feature of personal books may be all that a site requires for book customization, so hide the outline tab on all books.'),
    '#required' => TRUE,
  );
  $form['book_manager_hide_remove_tab'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide the remove tab on all books'),
    '#default_value' => variable_get('book_manager_hide_remove_tab', 0),
    '#description' => t('The <em>Customize Book</em> feature of personal books may be all that a site requires for book customization, so hide the remove tab on all books.'),
    '#required' => TRUE,
  );

  $form['book_manager_personal_book_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Default new books to personal'),
    '#default_value' => variable_get('book_manager_personal_book_default', 0),
    '#description' => t('This setting only affects users that have both "create personal books" and "create books" permissions. If a user has both of these permissions then they can choose if books they create are personal books or not. This setting governs the default option for users.'),
  );

  return system_settings_form($form);
}
