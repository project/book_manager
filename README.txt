README.txt for Book Manager 6.x-2.x
===================================
The Book Manager module allows users to create personal books.

Book Manager depends on the core Book module, and can be used alongside
it, which means a site can have some books that are personal and other
books that are not personal.

A personal book is one in which only the book owner may add pages.  Book
Manager allows users to access the drag-and-drop outlining feature
for their own personal books.  Normally this feature is only available
to users with administer book outlines rights.

INSTALLATION
============
- Enable the core Book module if it's not already enabled.
- Enable the Book Manager module.

CONFIGURATION	
=============	
- At admin/content/book-manager you can turn on some optional features:
-- "Show create links for each allowed book content type" replaces the
core book module's "Add child page" link (which adds the default book
page type) with Add content-type links for each content type that is
allowed to appear in a book.
-- "Hide the outline tab on all books"
-- "Hide the remove tab on all books"
-- "Default new books to personal" This setting only affects users that 
have both "create personal books" and "create books" permissions. If a 
user has both of these permissions then they can choose if books they 
create are personal books or not. This setting governs the default 
option for users.

PERMISSIONS
===========
- At admin/user/permissions you can assign the following Book Manager
permissions:
-- "add content to personal books" - Users with this permission who do
not have "add content to books" will only be able to add content to their
own personal books
-- "create personal books" - Users with this permission who do not have
"create new books" will only be able to create personal books.
-- "access book manager" - Book manager provides an interface to see personal
books at book_manager and book_manager/%uid this permission governs whether 
a user has access to those locations.
-- "book manager patch process" - As of book_manager 2.2 there is a new batch
operation that keeps nodes in a book with consistent settings. Giving users 
this permission will allow them to keep book nodes in sync. As of this time
integration is in place for the following modules:
    * og
    * og_access
    * og_access_roles
Additional modules may include book_manager integration by implementing
hook_book_manager_batch_operate()


NOTES
=====
- Users who have permissions to create both personal books and non-personal
books will see a "Make personal" checkbox in the Outline fieldset that allows
them to make the book a personal book.  Users who only have permissions to
create personal books will not see the checkbox -- all books created by
those users will automatically be personal books.
- There is a known issue with core and re-ordering books with more than 30
pages. Please see http://drupal.org/node/589440, comment 25 includes a 
backported patch for Drupal 6.

VIEWS
=====
- Book manager relies on the standard views book module integration to do 
most of the heavy lifting. Book manager does define one views relationship
this relationship is meant to use "Book: Top Level Book" as a target 
relationship. If you require the book manager "Book: Personal Book"
relationship then your views content will be filtered such that the only
content displayed will be in personal books.

CREDITS
=======
Book Manager was developed by the friendly primates at FunnyMonkey.
Development was sponsored and guided by Flat World Knowledge.
