Drupal.behaviors.bookManagerPersonalToggle = function() {
  if ($('#edit-book-bid').val() == 'new' || $('#edit-book-bid').val() == Drupal.settings.book_manager.nid) {
    $('#edit-book-ispersonal-wrapper').css('display', 'inline');
  }
  else {
    $('#edit-book-ispersonal-wrapper').css('display', 'none');
  }
}

Drupal.behaviors.bookManagerBookChange = function() {
  Drupal.behaviors.bookManagerPersonalToggle();
  $('#edit-book-bid').change(Drupal.behaviors.bookManagerPersonalToggle);
}


